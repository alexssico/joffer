<?php
require __DIR__ . '/vendor/autoload.php';

/**
 * Routing
 */
// Create Router instance
$router = new \Bramus\Router\Router();


$router->get('/', '\App\Controllers\User@index');
$router->put('/users', '\App\Controllers\User@update');
$router->get('/users/(\d+)', '\App\Controllers\User@show');
$router->post('/users', '\App\Controllers\User@add');
$router->delete('/users', '\App\Controllers\User@remove');

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
    return \Core\Response::view('errors/404');
});

$router->run();


//2. написати rest api з методами: створення, редагування, видалення, вибірка записів користувачів (забезпечити валідацію даних, і повернення помилок при запитах).
//3. написати примітивний mvc-двіжок (використати namespace), на якому реалізується сайт із сторінками для демонстрації роботи пункту 2 (використати ajax, відображення помилок при запитах).
