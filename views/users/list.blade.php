@extends('base')
@section('content')
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">FirstName</th>
            <th scope="col">LastName</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
            <tr id="raw-{{ $user->id }}">
                <th scope="row">{{ $user->id  }}</th>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" data-id="{{ $user->id }}" class="btn btn-secondary js-edit">Edit</button>
                        <button type="button" data-id="{{ $user->id }}" class="btn btn-danger js-remove-id" data-toggle="modal" data-target="#removeModal">Delete</button>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap" href="#" role="button">Add</a>

    @include('users.modal.add')
    @include('users.modal.edit')
    @include('users.modal.remove')
@stop
@section('scripts')
<script>
    $( document ).ready(function() {

        $(document).on("click", ".js-remove-id", function(){
            $(".js-remove").data('id', $(this).data('id'));
        });
        $(document).on("click", ".js-edit", function(){
            let id = $(this).data('id');
            $.ajax({
                url: 'users/' + id,
                type: 'GET',
                dataType: 'json',
                success: function(response) {
                    if (response.success){
                       let form = $('#userEdit');

                        $.each(response.data, function(index, value){

                            let elem = form.find("[name='" + index + "']");

                            if ( elem.length) {
                                elem.val(value);
                            }
                        });

                        $('#editModal').modal('show');
                    } else {
                        console.log(response.errors);
                    }
                }
            });
        });

        $(document).on("click", ".js-remove", function(e){
            let btn = $(this);
            $.ajax({
                data: {
                    id: btn.data('id')
                },
                url: '/users',
                type: 'DELETE',
                success: function(response) {
                    if(response) {
                        $('#raw-' + btn.data('id')).remove();
                    }
                }
            });
        });

        $(document).on("click", ".js-btn", function(e){
            e.preventDefault();

            let btn = $(this);

            $.ajax({
                url: btn.data('href'),
                type: btn.data('type'),
                data: $('#' + btn.data('form')).serialize(),
                dataType : 'json',
                success: function(response) {

                    if (response.success){
                         location.reload();
                    } else {
                        // TODO bootstrap alert
                        console.log(response.errors);
                    }
                }
            });
        });


    });
</script>
    @stop
