<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="userEdit">
                    <div class="form-group">
                        <label for="exampleInputfirstName">First Name</label>
                        <input name="firstname" type="text" class="form-control" id="exampleInputfirstName">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Last Name</label>
                        <input name="lastname" type="text" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input name="email" type="email" class="form-control" id="exampleInputEmail1" >
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Phone</label>
                        <input name="phone" type="number" class="form-control" id="exampleInputPassword1">
                    </div>
                    <input name="id"  type="hidden" class="form-control js-edit-id" id="exampleInputPassword1">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary js-btn" data-type="PUT" data-href="/users" data-form="userEdit">Update</button>
            </div>
        </div>
    </div>
</div>

