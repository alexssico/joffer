<?php

namespace Core;

use Rakit\Validation\Validator;

/**
 * Class Controller
 * @package Core
 */
abstract class Controller
{

    protected $validator;

    function __construct()
    {
        $this->validator =  new Validator;
    }

}
