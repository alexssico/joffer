<?php

namespace Core;

use Core\Traits\Singelton;
use PDO;
use App\Config;

/**
 * Class Model
 * @package Core
 */
abstract class Model
{
    protected static $db = null;
    /**
     * @return PDO|null
     */
    protected static function getDB()
    {
        if (static::$db === null) {

            try {
                $dsn = 'mysql:host=' . Config::DB_HOST . ';dbname=' . Config::DB_NAME . ';charset=utf8';
                $db = new PDO($dsn, Config::DB_USER, Config::DB_PASSWORD);

                // Throw an Exception when an error occurs
                $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

                static::$db = $db;
            } catch (\PDOException $e ){
                Response::view('errors/500',[
                    'errors' => $e->getMessage()
                ]);

                exit;
            }

        }

        return static::$db;
    }
}
