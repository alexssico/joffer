<?php

namespace Core;

use Jenssegers\Blade\Blade;

/**
 * Class View
 * @package Core
 */
class View
{
    /** TODO винести в конфиг  */

    /** @var string  */
    private static $viewsPath = 'views';

    /** @var string  */
    private static $cachePath = 'cache';


    /**
     * @param string $view
     * @param array $data
     * @return string
     */
    public static function render(string $view, array $data = [])
    {
        $blade = new Blade(static::$viewsPath, static::$cachePath);
        return $blade->make($view, $data)->render();
    }
}
