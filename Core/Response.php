<?php


namespace Core;

/**
 * Class Response
 * @package Core
 */
class Response
{
    /**
     * @param $data
     */
    public static function json($data){
        echo json_encode($data);
    }

    /**
     * @param string $view
     * @param array $data
     */
    public static function view(string $view, array $data = []){
        echo View::render($view, $data);
    }
}
