<?php


namespace Core;


/**
 * Class Request
 * @package Core
 */
class Request
{

    /**
     * @return mixed
     */
    public static function get(){
        return $_GET;
    }

    /**
     * @return array
     */
    public static function post(){
        $post = [];
        parse_str(file_get_contents('php://input'), $post);
        return $post;
    }
}
