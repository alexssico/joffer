<?php

namespace App\Controllers;

use App\Models\User as UserModel;
use Core\Controller;
use Core\Request;
use Core\Response;

/**
 * Class User
 * @package App\Controllers
 */
class User extends Controller
{

    /**
     *
     */
    public function index()
    {
        $users = UserModel::getAll();
        return Response::view( 'users.list', [
            'users' => $users
        ]);
    }

    /**
     *
     */
    public function add()
    {
        $validation = $this->validator->validate(Request::post(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required'
        ]);

        $data = [];

        if ( $validation->fails()) {
            $data['success'] = false;
            $data['errors'] = $validation->errors()->firstOfAll();
        } else {
            $data['success'] = UserModel::store(Request::post());

        }

        return Response::json($data);
    }


    public function update(){
        $validation = $this->validator->validate(Request::post(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'id' => 'required|numeric'
        ]);

        $data = [];

        if ( $validation->fails()) {
            $data['success'] = false;
            $data['errors'] = $validation->errors()->firstOfAll();
        } else {
            $data['success'] = UserModel::update(Request::post());

        }

        return Response::json($data);
    }


    public function show($id){
        $validation = $this->validator->validate(['id' => $id], [
            'id' => 'required|numeric'
        ]);

        $data = [];
        $data['success'] = false;

        if ( $validation->fails()) {
            $data['errors'] = $validation->errors()->firstOfAll();
        } else {

            $userData =  UserModel::getUser($id);

            if ($userData) {
                $data['success'] = true;
                $data['data'] = $userData;
            }
        }

        return Response::json($data);
    }

    /**
     *
     */
    public function remove()
    {
        $validation = $this->validator->validate(Request::post(), [
            'id' => 'required|numeric',
        ]);

        $data = [];

        if ( $validation->fails()) {
            $data['success'] = false;
            $data['errors'] = $validation->errors()->firstOfAll();
        } else {
            $data['success'] = UserModel::remove(Request::post()['id']);

        }

        return Response::json($data);
    }
}
