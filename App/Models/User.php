<?php

namespace App\Models;

use PDO;

/**
 * Class User
 * @package App\Models
 */
class User extends \Core\Model
{

    /**
     * @return array
     */
    public static function getAll()
    {
        $db = static::getDB();
        $stmt = $db->query('SELECT * FROM users');
        return $stmt->fetchAll(PDO::FETCH_CLASS);
    }

    /**
     * @return array
     */
        public static function getUser($id)
    {
        $db = static::getDB();
        $stmt = $db->prepare("SELECT * FROM users WHERE id=?");
        $stmt->execute([$id]);
        return $stmt->fetchObject ();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function remove($id){
        $db = static::getDB();
        $stmt = $db->prepare("DELETE FROM users WHERE id = :id");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function store($post){

        $db = static::getDB();
        $stmt = $db->prepare("INSERT INTO users (firstname, lastname, email, phone ) VALUES (:firstname, :lastname, :email, :phone)");
        return $stmt->execute($post);
    }

    /**
     * @param $id
     * @return bool
     */
    public static function update($post){

        $db = static::getDB();
        $stmt = $db->prepare("UPDATE users SET firstname = :firstname, lastname = :lastname, email= :email, phone = :phone WHERE id = :id");
        return $stmt->execute($post);
    }
}
