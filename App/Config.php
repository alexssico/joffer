<?php

namespace App;

/**
 * Class Config
 * @package App
 */
class Config
{
    /** @var string  */
    const DB_HOST = 'localhost';

    /** @var string  */
    const DB_NAME = 'joffer';

    /** @var string  */
    const DB_USER = 'root';

    /** @var string  */
    const DB_PASSWORD = '';

    /** @var bool  */
    const SHOW_ERRORS = true;
}
